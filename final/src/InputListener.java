import java.util.Scanner;
import java.util.function.BiConsumer;

public class InputListener {
    private final Scanner scanner;

    public InputListener(Scanner scanner) {
        this.scanner = scanner;
    }

    public void listen(BiConsumer<Command, String> consumer) {
        while (scanner.hasNextLine()) {
            String[] parts = scanner.nextLine().split(" ", 2);

            if (parts.length < 2) {
                System.out.println("You must enter a command and an input.");
            } else {
                try {
                    Command command = Command.valueOf(parts[0]);
                    consumer.accept(command, parts[1]);
                } catch (IllegalArgumentException e) {
                    System.out.println("Unrecognised command: " + parts[0]);
                }
            }
        }
    }
}
