public class Task {
    private final String description;

    private final Boolean complete;

    public Task(String description, Boolean complete) {
        this.description = description;
        this.complete = complete;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getComplete() {
        return complete;
    }
}
