import java.io.InputStream;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        TaskService taskService = new TaskService();

        InputStream inputStream = System.in;
        Scanner scanner = new Scanner(inputStream);
        InputListener inputListener = new InputListener(scanner);

        inputListener.listen((command, input) -> {
            switch (command) {
                case ADD:
                    taskService.add(input);
                    break;
                case COMPLETE:
                    taskService.complete(input);
                    break;
                case DELETE:
                    taskService.delete(input);
                    break;
            }
        });

        String firstTaskDescription = "Get milk";
        Boolean firstTaskCompleted = false;
        if (firstTaskCompleted) {
            System.out.println("TODO - " + firstTaskDescription);
        } else {
            System.out.println("DONE - " + firstTaskDescription);
        }

        String secondTaskDescription = "Get milk";
        Boolean secondTaskCompleted = false;
        if (secondTaskCompleted) {
            System.out.println("TODO - " + secondTaskDescription);
        } else {
            System.out.println("DONE - " + secondTaskDescription);
        }

        String thirdTaskDescription = "Get milk";
        Boolean thirdTaskCompleted = true;
        if (thirdTaskCompleted) {
            System.out.println("TODO - " + thirdTaskDescription);
        } else {
            System.out.println("DONE - " + thirdTaskDescription);
        }


    }
}
