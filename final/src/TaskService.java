import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class TaskService {
    private final Map<String, Task> tasks = new HashMap<>();

    public Map<String, Task> findAll() {
        return tasks;
    }

    public void add(String description) {
        String id = String.valueOf(tasks.size());
        tasks.put(id, new Task(description, false));
    }

    public void complete(String id) {
        Task task = tasks.get(id);
        if (task == null) {
            throw new NoSuchElementException("Cannot complete task " + id + " because it does not exist");
        }

        tasks.put(id, new Task(task.getDescription(), true));
    }

    public void delete(String id) {
        tasks.remove(id);
    }
}
