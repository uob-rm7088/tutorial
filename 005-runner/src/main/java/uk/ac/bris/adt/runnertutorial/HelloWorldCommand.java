package uk.ac.bris.adt.runnertutorial;

import com.beust.jcommander.Parameters;
import org.springframework.stereotype.Component;
import uk.ac.bris.adt.tools.runner.command.Command;

@Component
@Parameters(commandNames = "hello-world", commandDescription = "Prints hello world!")
public class HelloWorldCommand implements Command {
    @Override
    public void run() {
        System.out.println("Hello World!");
    }
}
