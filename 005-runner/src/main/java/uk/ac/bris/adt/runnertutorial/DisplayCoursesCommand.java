package uk.ac.bris.adt.runnertutorial;

import com.beust.jcommander.Parameters;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import uk.ac.bris.adt.tools.runner.command.Command;

import java.util.List;

@Component
@Parameters(commandNames = "display-courses", commandDescription = "Displays a list of UCAS courses")
public class DisplayCoursesCommand implements Command {
    private final JdbcTemplate jdbcTemplate;

    public DisplayCoursesCommand(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void run() {
        List<Course> courses = jdbcTemplate.query(
                "SELECT ID, NAME FROM BAU_STINT.COURSES_FOR_RAT",
                new BeanPropertyRowMapper<>(Course.class)
        );

        for (Course course : courses) {
            System.out.println(course.getId() + " " + course.getName());
        }
    }
}
