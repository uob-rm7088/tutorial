public class Application {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
        System.out.println(42);
        System.out.println(true);
        System.out.println(2 + 2);
        System.out.println(6 * 7);

        Integer result = 30 / 3;
        System.out.println(result);

        System.out.println(100 > 10);
        System.out.println(!"apples".equals("oranges"));

        if (10 == 10) {
            System.out.println("This message will be displayed");
        }

        String[] films = { "Star Wars: Episode IV - A New Hope", "Inception", "The Lion King" };
        for (String film : films) {
            System.out.println(film);
        }
    }
}
