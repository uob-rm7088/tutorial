public class Application {
    public static void main(String[] args) {
        String firstTaskDescription = "Get milk";
        Boolean firstTaskCompleted = false;
        if (firstTaskCompleted) {
            System.out.println("TODO - " + firstTaskDescription);
        } else {
            System.out.println("DONE - " + firstTaskDescription);
        }

        String secondTaskDescription = "Feed cat";
        Boolean secondTaskCompleted = false;
        if (secondTaskCompleted) {
            System.out.println("TODO - " + secondTaskDescription);
        } else {
            System.out.println("DONE - " + secondTaskDescription);
        }

        String thirdTaskDescription = "Pay bills";
        Boolean thirdTaskCompleted = true;
        if (thirdTaskCompleted) {
            System.out.println("TODO - " + thirdTaskDescription);
        } else {
            System.out.println("DONE - " + thirdTaskDescription);
        }
    }
}
